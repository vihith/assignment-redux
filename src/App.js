import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'

import Home from './components/home'
import Students from './components/students'

import "./components/students.css"
import "./components/home.css"
import Select from './components/select';


class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isClicked: true
    }
  }
  clickChange = () => {
    this.setState({
      isClicked: false
    })
  }
  render(){
  return (
    <div className="col-md-12">
      <BrowserRouter>
      <div className="col-md-4 home">
      <Route path='/'  render={(props) => {
        return <Home {...props} clickChange={this.clickChange}/>
      }}  />
      {this.state.isClicked && <Select />}

      </div>
      <div className="students offset-4">
        <Route path='/:classname' component={Students} exact={true} />
      </div>   
      </BrowserRouter>
    </div>
  )
  }
}

export default App