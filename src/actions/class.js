export const classes = (cls) => {
    return {
        type: 'CLASS',
        payload: cls
    }
}