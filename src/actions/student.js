export const students = (students) => {
    return {
        type: 'STUDENT',
        payload: students
    }
}