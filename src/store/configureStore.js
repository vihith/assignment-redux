import { createStore, combineReducers } from 'redux'

import classReducer  from '../reducers/class'
import studentsReducer from '../reducers/student'

const configureStore = () => {
    const store = createStore(combineReducers({
        cls : classReducer,
        students : studentsReducer
    }))
    return store
}

export default configureStore