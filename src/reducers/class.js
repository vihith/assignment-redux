const classReducer = ( state = [], action ) => {
    switch(action.type) {
        case 'CLASS' :
            return [...action.payload]
        default :
            return state
    }
}

export default classReducer