import React from 'react'
import { connect } from 'react-redux'


class Students extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isToggle: false
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

        average() {
            let result = 0
            for (let k in arguments) {
                result += arguments[k]
            }
            return parseInt(result / arguments.length)
        }
        classAverage(){
            let classAvg = 0
            this.props.students.forEach((student) => {
                classAvg += this.average(...Object.values(student.marks))
            })
            return parseInt(classAvg / this.props.students.length)
        }
        buttonClick(){
            console.log("button clicked",this.state.isToggle)
            this.setState((prevState) =>{
               return {isToggle : !prevState.isToggle}
            })
        }

        render(){
            return (
                this.props.students.length ? (
                    <div style={{ 'height': '800px' }}>
                        <h2 className="clsname"><b>{this.props.match.params.classname}</b><span><button className="button btn btn-dark" onClick={this.buttonClick} >Show / Hide Average</button></span></h2>
                        <p style={{'font-size': 'larger'}} className="studentname"><b>{this.props.students.length} Students</b></p>
                        {this.state.isToggle && <p className="">
                            <div className="clsaverage">
                            <b>Class Average Performance : {this.classAverage()}%</b>
                            </div>
                        </p>}
                        <hr className="hr" />
                        <ul>
                            {this.props.students.map((student, index) => {
                                return (
                                    <div className="card text-white bg-dark border-success mb-2 col-md-4" >
                                        <p><b>{student.name}</b> <span className="span" > Average: {this.average(...Object.values(student.marks))}
                                            %</span></p>
                                        <div>English <span className="subject">{student.marks.English}%</span>
                                            <div className="progress progressbars" >
                                                <div className="progress-bar" role="progressbar" style={{ 'width': `${student.marks.English}%` }} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">{student.marks.English}%
                                            </div>
                                            </div>

                                        </div>
                                        <p>Maths <span className="subject">{student.marks.Maths}%</span>
                                            <div className="progress progressbars" >
                                                <div className="progress-bar" role="progressbar" style={{ 'width': `${student.marks.Maths}%` }} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">{student.marks.Maths}%
                                            </div>
                                            </div>
                                        </p>
                                        <p>Science <span className="subject">{student.marks.Science}%</span>
                                            <div className="progress progressbars" >
                                                <div className="progress-bar" role="progressbar" style={{ 'width': `${student.marks.Science}%` }} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">{student.marks.Science}%
                                            </div>
                                            </div>
                                        </p></div>

                                )
                            })}

                        </ul>
                    </div>
                )
                    :
                    (
                        <div>
                            {this.props.history.push('/')}
                        </div>
                    )
            )
        }
}

    const mapStateToProps = (state) => {
        return {
            schools: state.schools,
            students: state.students
        }
    }
    export default connect(mapStateToProps)(Students)