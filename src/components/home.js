import React from 'react'
import { Link } from 'react-router-dom'

import schools from '../classroom_data.json'
import { connect } from 'react-redux'

import {students} from '../actions/student'
// import Students from './students'

import "./home.css"

class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            schools : schools,
            cls : {},
            isClicked : false
        }
        this.handleClick = this.handleClick.bind(this)
    }


    handleClick(e){
        let result = this.state.schools.find((cls) => {
            return cls.classname === e.target.name
        })
        this.setState({
            isClicked : true,
            cls : result
        })
        this.props.dispatch(students(result.students))
        this.props.clickChange()
        this.props.history.push(`/${e.target.name}`)
    }
    render(){
        // console.log(this.props)
        return(
            <div>
                <h2 className="headingcolor">School</h2>
                <ul className="unorderedlist active">
                {this.state.schools.map((school, index) => {
                    return <p key={index}><Link className="cls"  to={`${school.classname}`} name={school.classname} onClick={this.handleClick}>{school.classname} </Link></p>
                })}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        students: state.students
    }
}
export default connect(mapStateToProps) (Home)